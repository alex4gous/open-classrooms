#!/bin/bash

#Auteur : Corentin MULLER

#Version : 1.0
#Date (02/04/2020)

# Permet l'installation du serveur Nagios 4.4.5 (Supervision)
#OS referent teste : https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-9.6.0-amd64-netinst.iso
#Execution en tant qu'utilisateur root

# Installation serveur apache, php et dependances 
apt-get install apache2 php php-gd php-imap php-curl php-mcrypt

# Installation des librairie perl
apt-get install libxml-libxml-perl libnet-snmp-perl libperl-dev libnumber-format-perl libconfig-inifiles-perl libdatetime-perl libnet-dns-perl

# Installation des librairie graphique
apt-get install libpng-dev libjpeg-dev libgd-dev

# Installation des outils de compilation standard
apt-get install gcc make autoconf libc6 unzip

# Creation environnement Nagios 

# Creation d'un utilisateur
useradd -m -p $(openssl passwd nagios) nagios

# Creation du groupe nagcmd
groupadd nagcmd

# Integration de l'utilisateur nagios au groupe nagcmd
usermod -a -G nagcmd nagios

# Integration de l'utilisateur www-data au groupe nagcmd
usermod -a -G nagcmd www-data

# Creation d'un repertoire pour le stockage des telechargements (/home/nagios/downloads)
mkdir /home/nagios/downloads

# On se place dans le repertoir creer precedement (/home/nagios/downloads)
cd /home/nagios/downloads

# Telechargement des sources de nagios (4.4.5)
wget https://assets.nagios.com/downloads/nagioscore/releases/nagios-4.4.5.tar.gz

# Decompression des sources telechargee
tar -zxvf nagios-4.4.5.tar.gz

# On se place dans le repertoire nagios-4.4.5
cd nagios-4.4.5

# Preparation de la compilation des sources de nagios avec parametres 
./configure --with-httpd-conf=/etc/apache2/sites-enabled --with-command-group=nagcmd

# Compilation des sources 
make all

# Creation de l'arborescence Nagios
make install

# verification de l'arborescence nagios
# ls -lrtha /usr/local/nagios

# Installation du service Nagios (nagios.service dans systemd)
make install-daemoninit

# Installation pipe Nagios
make install-commandmode

# Installation des fichiers de configuration
make install-config

# Installation de l'interface web d'administration
make install-webconf

# Apache : activation des modules rewrite et cgi
a2enmod rewrite
a2enmod cgi

# Saisie utilisateur
echo "Entrez le mot de passe de connexion à l'administration web de nagios "
read pass


# Definition du mot de passe d'acces a l'administration web. 
htpasswd -cb /usr/local/nagios/etc/htpasswd.users nagiosadmin $pass

# Prise en compte de la configuration : redemarrage apache 
systemctl restart apache2

# Demarrage du service apache
systemctl start nagios

# Verification de l'execution du service nagios
systemctl status nagios 

# Message post execution 
echo " - URL d'accès nagios : http://<@ip>/nagios
       - Nom d'utilisateur : nagiosadmin
       - Mot de passe : $pass

       Etape suivante : 
       - Installer les plugin avec le script plugins.sh"